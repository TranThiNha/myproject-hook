Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com

1. Các chức năng đã làm được:
- Khi kích hoạt phím Shift, tự động vô hiệu hóa chuột trái (bấm chuột trái như không bấm). 
Chỉ trở về bình thường khi bấm phím Shift lại 1 lần nữa.
- Đưa các hàm xử lí hook vào dll.

2. Các luồng sự kiện chính:
- Màn hình hiện 1 button CLICK. 
- Khi nhấn chuột trái vào button sẽ hiển thị MessageBox thông báo chuột trái đang được nhấn.
- Nhấn phím Shift, sau đó nhấn chuột trái lại vào button, không có MessageBox hiển thị, chứng tỏ chuột trái đã bị vô hiệu hóa.
- Nhấn phím Shift, sau đó nhấn chuột trái lại vào button, hiển thị MessageBox thông báo chuột trái đang được nhấn, chứng tỏ chuột trái đã hết bị vô hiệu hóa.

3. Link Bitbucket: https://bitbucket.org/TranThiNha/myproject-hook

4. Link Youtube: https://youtu.be/WzIFk9rZC9E