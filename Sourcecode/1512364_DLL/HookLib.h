#pragma once

#ifdef HOOKLIBRARY_EXPORTS
#define HOOKLIBRARY_API __declspec(dllexport) 
#else
#define HOOKLIBRARY_API __declspec(dllimport) 
#endif

namespace HookDll
{
	class Hook
	{
	public:static HOOKLIBRARY_API LRESULT CALLBACK MyHookProc(int nCode, WPARAM wParam, LPARAM lParam);
		
	};
}


