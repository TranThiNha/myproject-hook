#include "stdafx.h"
#include "HookLib.h"

namespace HookDll
{
	LRESULT CALLBACK Hook::MyHookProc(int nCode, WPARAM wParam, LPARAM lParam)
	{
		if (wParam == WM_LBUTTONDOWN)
		{
			return true;
		}
		return CallNextHookEx(0, nCode, wParam, lParam);
	}
}